<?php


/**
 * Base class that represents a query for the 'cpdoccom' table.
 *
 * Tabla que contiene información referente a los documentos que registran un compromiso.
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:38 2015
 *
 * @method CpdoccomQuery orderByTipcom($order = Criteria::ASC) Order by the tipcom column
 * @method CpdoccomQuery orderByNomext($order = Criteria::ASC) Order by the nomext column
 * @method CpdoccomQuery orderByNomabr($order = Criteria::ASC) Order by the nomabr column
 * @method CpdoccomQuery orderByRefprc($order = Criteria::ASC) Order by the refprc column
 * @method CpdoccomQuery orderByAfeprc($order = Criteria::ASC) Order by the afeprc column
 * @method CpdoccomQuery orderByAfecom($order = Criteria::ASC) Order by the afecom column
 * @method CpdoccomQuery orderByAfedis($order = Criteria::ASC) Order by the afedis column
 * @method CpdoccomQuery orderByReqaut($order = Criteria::ASC) Order by the reqaut column
 * @method CpdoccomQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CpdoccomQuery groupByTipcom() Group by the tipcom column
 * @method CpdoccomQuery groupByNomext() Group by the nomext column
 * @method CpdoccomQuery groupByNomabr() Group by the nomabr column
 * @method CpdoccomQuery groupByRefprc() Group by the refprc column
 * @method CpdoccomQuery groupByAfeprc() Group by the afeprc column
 * @method CpdoccomQuery groupByAfecom() Group by the afecom column
 * @method CpdoccomQuery groupByAfedis() Group by the afedis column
 * @method CpdoccomQuery groupByReqaut() Group by the reqaut column
 * @method CpdoccomQuery groupById() Group by the id column
 *
 * @method CpdoccomQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CpdoccomQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CpdoccomQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CpdoccomQuery leftJoinCpcompro($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cpcompro relation
 * @method CpdoccomQuery rightJoinCpcompro($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cpcompro relation
 * @method CpdoccomQuery innerJoinCpcompro($relationAlias = null) Adds a INNER JOIN clause to the query using the Cpcompro relation
 *
 * @method CpdoccomQuery leftJoinCpcomext($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cpcomext relation
 * @method CpdoccomQuery rightJoinCpcomext($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cpcomext relation
 * @method CpdoccomQuery innerJoinCpcomext($relationAlias = null) Adds a INNER JOIN clause to the query using the Cpcomext relation
 *
 * @method Cpdoccom findOne(PropelPDO $con = null) Return the first Cpdoccom matching the query
 * @method Cpdoccom findOneOrCreate(PropelPDO $con = null) Return the first Cpdoccom matching the query, or a new Cpdoccom object populated from the query conditions when no match is found
 *
 * @method Cpdoccom findOneByTipcom(string $tipcom) Return the first Cpdoccom filtered by the tipcom column
 * @method Cpdoccom findOneByNomext(string $nomext) Return the first Cpdoccom filtered by the nomext column
 * @method Cpdoccom findOneByNomabr(string $nomabr) Return the first Cpdoccom filtered by the nomabr column
 * @method Cpdoccom findOneByRefprc(string $refprc) Return the first Cpdoccom filtered by the refprc column
 * @method Cpdoccom findOneByAfeprc(string $afeprc) Return the first Cpdoccom filtered by the afeprc column
 * @method Cpdoccom findOneByAfecom(string $afecom) Return the first Cpdoccom filtered by the afecom column
 * @method Cpdoccom findOneByAfedis(string $afedis) Return the first Cpdoccom filtered by the afedis column
 * @method Cpdoccom findOneByReqaut(string $reqaut) Return the first Cpdoccom filtered by the reqaut column
 *
 * @method array findByTipcom(string $tipcom) Return Cpdoccom objects filtered by the tipcom column
 * @method array findByNomext(string $nomext) Return Cpdoccom objects filtered by the nomext column
 * @method array findByNomabr(string $nomabr) Return Cpdoccom objects filtered by the nomabr column
 * @method array findByRefprc(string $refprc) Return Cpdoccom objects filtered by the refprc column
 * @method array findByAfeprc(string $afeprc) Return Cpdoccom objects filtered by the afeprc column
 * @method array findByAfecom(string $afecom) Return Cpdoccom objects filtered by the afecom column
 * @method array findByAfedis(string $afedis) Return Cpdoccom objects filtered by the afedis column
 * @method array findByReqaut(string $reqaut) Return Cpdoccom objects filtered by the reqaut column
 * @method array findById(int $id) Return Cpdoccom objects filtered by the id column
 *
 * @package    propel.generator.lib.model.presupuesto.om
 */
abstract class BaseCpdoccomQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCpdoccomQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cpdoccom', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CpdoccomQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CpdoccomQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CpdoccomQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CpdoccomQuery) {
            return $criteria;
        }
        $query = new CpdoccomQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cpdoccom|Cpdoccom[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CpdoccomPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CpdoccomPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpdoccom A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpdoccom A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "tipcom", "nomext", "nomabr", "refprc", "afeprc", "afecom", "afedis", "reqaut", "id" FROM "cpdoccom" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cpdoccom();
            $obj->hydrate($row);
            CpdoccomPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cpdoccom|Cpdoccom[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cpdoccom[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CpdoccomPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CpdoccomPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the tipcom column
     *
     * Example usage:
     * <code>
     * $query->filterByTipcom('fooValue');   // WHERE tipcom = 'fooValue'
     * $query->filterByTipcom('%fooValue%'); // WHERE tipcom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipcom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByTipcom($tipcom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipcom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipcom)) {
                $tipcom = str_replace('*', '%', $tipcom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::TIPCOM, $tipcom, $comparison);
    }

    /**
     * Filter the query on the nomext column
     *
     * Example usage:
     * <code>
     * $query->filterByNomext('fooValue');   // WHERE nomext = 'fooValue'
     * $query->filterByNomext('%fooValue%'); // WHERE nomext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomext The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByNomext($nomext = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomext)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomext)) {
                $nomext = str_replace('*', '%', $nomext);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::NOMEXT, $nomext, $comparison);
    }

    /**
     * Filter the query on the nomabr column
     *
     * Example usage:
     * <code>
     * $query->filterByNomabr('fooValue');   // WHERE nomabr = 'fooValue'
     * $query->filterByNomabr('%fooValue%'); // WHERE nomabr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomabr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByNomabr($nomabr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomabr)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomabr)) {
                $nomabr = str_replace('*', '%', $nomabr);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::NOMABR, $nomabr, $comparison);
    }

    /**
     * Filter the query on the refprc column
     *
     * Example usage:
     * <code>
     * $query->filterByRefprc('fooValue');   // WHERE refprc = 'fooValue'
     * $query->filterByRefprc('%fooValue%'); // WHERE refprc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refprc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByRefprc($refprc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refprc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refprc)) {
                $refprc = str_replace('*', '%', $refprc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::REFPRC, $refprc, $comparison);
    }

    /**
     * Filter the query on the afeprc column
     *
     * Example usage:
     * <code>
     * $query->filterByAfeprc('fooValue');   // WHERE afeprc = 'fooValue'
     * $query->filterByAfeprc('%fooValue%'); // WHERE afeprc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $afeprc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByAfeprc($afeprc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($afeprc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $afeprc)) {
                $afeprc = str_replace('*', '%', $afeprc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::AFEPRC, $afeprc, $comparison);
    }

    /**
     * Filter the query on the afecom column
     *
     * Example usage:
     * <code>
     * $query->filterByAfecom('fooValue');   // WHERE afecom = 'fooValue'
     * $query->filterByAfecom('%fooValue%'); // WHERE afecom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $afecom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByAfecom($afecom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($afecom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $afecom)) {
                $afecom = str_replace('*', '%', $afecom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::AFECOM, $afecom, $comparison);
    }

    /**
     * Filter the query on the afedis column
     *
     * Example usage:
     * <code>
     * $query->filterByAfedis('fooValue');   // WHERE afedis = 'fooValue'
     * $query->filterByAfedis('%fooValue%'); // WHERE afedis LIKE '%fooValue%'
     * </code>
     *
     * @param     string $afedis The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByAfedis($afedis = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($afedis)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $afedis)) {
                $afedis = str_replace('*', '%', $afedis);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::AFEDIS, $afedis, $comparison);
    }

    /**
     * Filter the query on the reqaut column
     *
     * Example usage:
     * <code>
     * $query->filterByReqaut('fooValue');   // WHERE reqaut = 'fooValue'
     * $query->filterByReqaut('%fooValue%'); // WHERE reqaut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reqaut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterByReqaut($reqaut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reqaut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reqaut)) {
                $reqaut = str_replace('*', '%', $reqaut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::REQAUT, $reqaut, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CpdoccomPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CpdoccomPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpdoccomPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Cpcompro object
     *
     * @param   Cpcompro|PropelObjectCollection $cpcompro  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CpdoccomQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCpcompro($cpcompro, $comparison = null)
    {
        if ($cpcompro instanceof Cpcompro) {
            return $this
                ->addUsingAlias(CpdoccomPeer::TIPCOM, $cpcompro->getTipcom(), $comparison);
        } elseif ($cpcompro instanceof PropelObjectCollection) {
            return $this
                ->useCpcomproQuery()
                ->filterByPrimaryKeys($cpcompro->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCpcompro() only accepts arguments of type Cpcompro or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cpcompro relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function joinCpcompro($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cpcompro');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cpcompro');
        }

        return $this;
    }

    /**
     * Use the Cpcompro relation Cpcompro object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CpcomproQuery A secondary query class using the current class as primary query
     */
    public function useCpcomproQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCpcompro($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cpcompro', 'CpcomproQuery');
    }

    /**
     * Filter the query by a related Cpcomext object
     *
     * @param   Cpcomext|PropelObjectCollection $cpcomext  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CpdoccomQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCpcomext($cpcomext, $comparison = null)
    {
        if ($cpcomext instanceof Cpcomext) {
            return $this
                ->addUsingAlias(CpdoccomPeer::TIPCOM, $cpcomext->getTipcom(), $comparison);
        } elseif ($cpcomext instanceof PropelObjectCollection) {
            return $this
                ->useCpcomextQuery()
                ->filterByPrimaryKeys($cpcomext->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCpcomext() only accepts arguments of type Cpcomext or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cpcomext relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function joinCpcomext($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cpcomext');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cpcomext');
        }

        return $this;
    }

    /**
     * Use the Cpcomext relation Cpcomext object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CpcomextQuery A secondary query class using the current class as primary query
     */
    public function useCpcomextQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCpcomext($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cpcomext', 'CpcomextQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Cpdoccom $cpdoccom Object to remove from the list of results
     *
     * @return CpdoccomQuery The current query, for fluid interface
     */
    public function prune($cpdoccom = null)
    {
        if ($cpdoccom) {
            $this->addUsingAlias(CpdoccomPeer::ID, $cpdoccom->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
