<?php



/**
 * This class defines the structure of the 'opordemp' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:42 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.tesoreria.map
 */
class OpordempTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.tesoreria.map.OpordempTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('opordemp');
        $this->setPhpName('Opordemp');
        $this->setClassname('Opordemp');
        $this->setPackage('lib.model.tesoreria');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('numord', 'Numord', 'VARCHAR', true, 8, null);
        $this->addColumn('cedrif', 'Cedrif', 'VARCHAR', true, 15, null);
        $this->addColumn('montot', 'Montot', 'NUMERIC', false, 14, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // OpordempTableMap
