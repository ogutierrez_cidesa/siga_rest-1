<?php

/**
 * Subclass for representing a row from the 'fapresup'.
 *
 *
 *
 * @package    Roraima
 * @subpackage lib.model
 * @author     $Author: dmartinez $ <desarrollo@cidesa.com.ve>
 * @version SVN: $Id: Fapresup.php 57499 2014-06-26 20:50:00Z dmartinez $
 * 
 * @copyright  Copyright 2007, Cide S.A.
 * @license    http://opensource.org/licenses/gpl-2.0.php GPLv2
 */
class Fapresup extends BaseFapresup
{
	public $obj = array();
  protected $concre = "";
  protected $obj2=array();
  protected $filactrec="";
  protected $totrec="0,00";
  protected $trajo = "";
  protected $obj3=array();
  protected $filactcon="";

    public function getRifpro()
    {
        return Herramientas::getX('CODPRO','Facliente','Rifpro',self::getCodcli());
    }

    public function getNompro()
    {
  	    return Herramientas::getX('CODPRO','Facliente','Nompro',self::getCodcli());
    }

    public function getDirpro()
    {
  	    return Herramientas::getX('CODPRO','Facliente','Dirpro',self::getCodcli());
    }

    public function getTelpro()
    {
  	    return Herramientas::getX('CODPRO','Facliente','Telpro',self::getCodcli());
    }

    public function getTipcte()
    {
  	    $fatipcte_id = Herramientas::getX('CODPRO','Facliente','Fatipcte_id',self::getCodcli());
            return H::getX('id', 'Fatipcte', 'nomtipcte', $fatipcte_id);
    }

    public function getCodtipcli()
    {
  	    return Herramientas::getX('CODPRO','Facliente','Fatipcte_id',self::getCodcli());
    }

    public function getValmon($val=false)
  {

    if($val) return number_format($this->valmon,6,',','.');
    else return $this->valmon;

  }
  
    public function setValmon($v)
    {

        if ($this->valmon !== $v) {
            $this->valmon = Herramientas::toFloat($v,6);
            $this->modifiedColumns[] = FapresupPeer::VALMON;
          }  
    } 

    public function getDesdirec()
    {
        return H::getX('CODDIREC','cadefdirec','Desdirec',self::getCoddirec());
    } 

    public function TienePedFac(){
      $tiene='N';
      
      $q= new Criteria();
      $q->add(FapedidoPeer::REFPED,self::getRefpre());
      $q->add(FapedidoPeer::TIPREF,'PR');
      $resulq= FapedidoPeer::doSelectOne($q);
      if ($resulq)
        $tiene='S';
      else{
        $a= new Criteria();
        $a->add(FafacturPeer::TIPREF,'E');
        $a->add(FaartfacPeer::CODREF,self::getRefpre());
        $a->addJoin(FaartfacPeer::REFFAC,FafacturPeer::REFFAC);
        $resula= FaartfacPeer::doSelectOne($a);
        if ($resula)
          $tiene='S';
      }
      return $tiene;
    }       

}
