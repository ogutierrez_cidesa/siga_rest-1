<?php


/**
 * Base class that represents a query for the 'faforpag' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:48 2015
 *
 * @method FaforpagQuery orderByReffac($order = Criteria::ASC) Order by the reffac column
 * @method FaforpagQuery orderByTippag($order = Criteria::ASC) Order by the tippag column
 * @method FaforpagQuery orderByNropag($order = Criteria::ASC) Order by the nropag column
 * @method FaforpagQuery orderByNomban($order = Criteria::ASC) Order by the nomban column
 * @method FaforpagQuery orderByMonpag($order = Criteria::ASC) Order by the monpag column
 * @method FaforpagQuery orderByNumero($order = Criteria::ASC) Order by the numero column
 * @method FaforpagQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FaforpagQuery groupByReffac() Group by the reffac column
 * @method FaforpagQuery groupByTippag() Group by the tippag column
 * @method FaforpagQuery groupByNropag() Group by the nropag column
 * @method FaforpagQuery groupByNomban() Group by the nomban column
 * @method FaforpagQuery groupByMonpag() Group by the monpag column
 * @method FaforpagQuery groupByNumero() Group by the numero column
 * @method FaforpagQuery groupById() Group by the id column
 *
 * @method FaforpagQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FaforpagQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FaforpagQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Faforpag findOne(PropelPDO $con = null) Return the first Faforpag matching the query
 * @method Faforpag findOneOrCreate(PropelPDO $con = null) Return the first Faforpag matching the query, or a new Faforpag object populated from the query conditions when no match is found
 *
 * @method Faforpag findOneByReffac(string $reffac) Return the first Faforpag filtered by the reffac column
 * @method Faforpag findOneByTippag(int $tippag) Return the first Faforpag filtered by the tippag column
 * @method Faforpag findOneByNropag(string $nropag) Return the first Faforpag filtered by the nropag column
 * @method Faforpag findOneByNomban(string $nomban) Return the first Faforpag filtered by the nomban column
 * @method Faforpag findOneByMonpag(string $monpag) Return the first Faforpag filtered by the monpag column
 * @method Faforpag findOneByNumero(string $numero) Return the first Faforpag filtered by the numero column
 *
 * @method array findByReffac(string $reffac) Return Faforpag objects filtered by the reffac column
 * @method array findByTippag(int $tippag) Return Faforpag objects filtered by the tippag column
 * @method array findByNropag(string $nropag) Return Faforpag objects filtered by the nropag column
 * @method array findByNomban(string $nomban) Return Faforpag objects filtered by the nomban column
 * @method array findByMonpag(string $monpag) Return Faforpag objects filtered by the monpag column
 * @method array findByNumero(string $numero) Return Faforpag objects filtered by the numero column
 * @method array findById(int $id) Return Faforpag objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFaforpagQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFaforpagQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Faforpag', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FaforpagQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FaforpagQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FaforpagQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FaforpagQuery) {
            return $criteria;
        }
        $query = new FaforpagQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Faforpag|Faforpag[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FaforpagPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FaforpagPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Faforpag A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Faforpag A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "reffac", "tippag", "nropag", "nomban", "monpag", "numero", "id" FROM "faforpag" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Faforpag();
            $obj->hydrate($row);
            FaforpagPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Faforpag|Faforpag[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Faforpag[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FaforpagPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FaforpagPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the reffac column
     *
     * Example usage:
     * <code>
     * $query->filterByReffac('fooValue');   // WHERE reffac = 'fooValue'
     * $query->filterByReffac('%fooValue%'); // WHERE reffac LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reffac The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByReffac($reffac = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reffac)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reffac)) {
                $reffac = str_replace('*', '%', $reffac);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FaforpagPeer::REFFAC, $reffac, $comparison);
    }

    /**
     * Filter the query on the tippag column
     *
     * Example usage:
     * <code>
     * $query->filterByTippag(1234); // WHERE tippag = 1234
     * $query->filterByTippag(array(12, 34)); // WHERE tippag IN (12, 34)
     * $query->filterByTippag(array('min' => 12)); // WHERE tippag >= 12
     * $query->filterByTippag(array('max' => 12)); // WHERE tippag <= 12
     * </code>
     *
     * @param     mixed $tippag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByTippag($tippag = null, $comparison = null)
    {
        if (is_array($tippag)) {
            $useMinMax = false;
            if (isset($tippag['min'])) {
                $this->addUsingAlias(FaforpagPeer::TIPPAG, $tippag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tippag['max'])) {
                $this->addUsingAlias(FaforpagPeer::TIPPAG, $tippag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaforpagPeer::TIPPAG, $tippag, $comparison);
    }

    /**
     * Filter the query on the nropag column
     *
     * Example usage:
     * <code>
     * $query->filterByNropag('fooValue');   // WHERE nropag = 'fooValue'
     * $query->filterByNropag('%fooValue%'); // WHERE nropag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nropag The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByNropag($nropag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nropag)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nropag)) {
                $nropag = str_replace('*', '%', $nropag);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FaforpagPeer::NROPAG, $nropag, $comparison);
    }

    /**
     * Filter the query on the nomban column
     *
     * Example usage:
     * <code>
     * $query->filterByNomban('fooValue');   // WHERE nomban = 'fooValue'
     * $query->filterByNomban('%fooValue%'); // WHERE nomban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomban The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByNomban($nomban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomban)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomban)) {
                $nomban = str_replace('*', '%', $nomban);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FaforpagPeer::NOMBAN, $nomban, $comparison);
    }

    /**
     * Filter the query on the monpag column
     *
     * Example usage:
     * <code>
     * $query->filterByMonpag(1234); // WHERE monpag = 1234
     * $query->filterByMonpag(array(12, 34)); // WHERE monpag IN (12, 34)
     * $query->filterByMonpag(array('min' => 12)); // WHERE monpag >= 12
     * $query->filterByMonpag(array('max' => 12)); // WHERE monpag <= 12
     * </code>
     *
     * @param     mixed $monpag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByMonpag($monpag = null, $comparison = null)
    {
        if (is_array($monpag)) {
            $useMinMax = false;
            if (isset($monpag['min'])) {
                $this->addUsingAlias(FaforpagPeer::MONPAG, $monpag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monpag['max'])) {
                $this->addUsingAlias(FaforpagPeer::MONPAG, $monpag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaforpagPeer::MONPAG, $monpag, $comparison);
    }

    /**
     * Filter the query on the numero column
     *
     * Example usage:
     * <code>
     * $query->filterByNumero('fooValue');   // WHERE numero = 'fooValue'
     * $query->filterByNumero('%fooValue%'); // WHERE numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numero The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterByNumero($numero = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numero)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numero)) {
                $numero = str_replace('*', '%', $numero);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FaforpagPeer::NUMERO, $numero, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FaforpagPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FaforpagPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaforpagPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Faforpag $faforpag Object to remove from the list of results
     *
     * @return FaforpagQuery The current query, for fluid interface
     */
    public function prune($faforpag = null)
    {
        if ($faforpag) {
            $this->addUsingAlias(FaforpagPeer::ID, $faforpag->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
